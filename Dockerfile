FROM openjdk:11
ADD target/*.jar customer-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","customer-0.0.1-SNAPSHOT.jar"]